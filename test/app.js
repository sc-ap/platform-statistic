'use strict';
(function(app) {

    app.constant('PAGES', ['page1', 'page2', 'page3']);

    app.config(function($routeProvider, $locationProvider, PAGES) {

        PAGES.forEach(function(page) {
            $routeProvider.when('/' + page, {
                template: '<h2>Page {{name}}</h2>',
                controller: function($scope, name) {
                    $scope.name = name;
                },
                resolve: {
                    name: function() {
                        return page;
                    }
                }
            });
        });
    });

    app.run(function($statistic) {
        $statistic.start({
            trackingUrl: 'http://mnasyrov:8080/statistic/api/track',
            aid: 'TEST-300'
        });
    });

    app.controller('RootController', function ($scope, PAGES, $statistic) {
        $scope.pages = PAGES;
        $scope.click = function() {
            $statistic.action('group', 'action', 'name', 'value');
        }
    });

})(window.angular.module('app', [
    'ngRoute',
    'platform.statistic'
]));