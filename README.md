----

# ВНИМАНИЕ #
**Проект переехал на https://git.idvp.net/ap/platform-statistic**

----




Platform Statistic JavaScript Client
=========================================================

## Install

```
bower install --save https://bitbucket.org/sc-ap/platform-statistic.git
```


## Usage

```javascript
angular.module('app', ['platform.statistic']);
```

## Configure

```javascript
angular.module('app').run(function($statistic) {
    $statistic.start({
        trackingUrl: 'URL_TO_BACKEND',
        aid: 'APP_CODE'
    });
});
```

## Actions via JS

```javascript
$statistic.action('group', 'action', 'name', 'value');
```

## Actions via directives

```html
<button ps-category="cat1">Category</button>
<button ps-category="cat2" ps-action="action1">Category and Action</button>
<button ps-category="cat3" ps-action="action2" ps-name="name1">Category and Action and Name</button>
<button ps-category="cat4" ps-action="action3" ps-value="value1">Category and Action and Value</button>
<button ps-category="cat5" ps-action="action4" ps-name="name2" value="2">Category and Action and Name and Value</button>
```

## Transitions

Transitions are tracked automatically.

## Errors

```javascript
angular.module('app').controller(function($statistic) {
    try {
      // do something...
    } catch(e) {
        $statistic.error(500, e.name); // code and message
    }
});
```

## Configuration defaults

```javascript
{
	trackingUrl: null, // Url to backend
	aid: null, // application unique id
	delay: 100, // delay before send
	beforeSendProvider: function(type, data) {  // before send info to server
	    return data;
	},
	titleResolver: function() {
		return (document[0]||{}).title;
	}
}
```