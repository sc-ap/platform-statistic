'use strict';
(function(app, angular) {

    function StatisticService(root, http, window, location, timeout, document) {
        var config = {
            started: false,
            trackingUrl: null,
            aid: null,
            delay: 100,
            referrer: document.referrer,
            localStoragePrefix: 'platform.statistic.',
            beforeSendProvider: function() {},
            titleResolver: function() {
                return (document[0]||{}).title;
            }
        };

        var titlesCache = {};

        function getCurrentTitle(url) {
            var title = config.titleResolver();
            titlesCache[url] = title;
            return title;
        }

        function getTitleByUrl(url) {
            return titlesCache[url];
        }

        this.start = function(options) {
            if (config.started) return;
            angular.merge(config, options, {
                clientCode: lsGet('client'),
                sessionCode: generateClientCode(),
                res: calculateScreenResolution(),
                started: true
            });

            if (!config.clientCode || !config.clientCode.length) {
                config.clientCode = generateClientCode();
                lsSet('client', config.clientCode);
            }

            root.$on('$locationChangeSuccess', function(evt, newUrl, oldUrl) {
                transition(oldUrl, newUrl);
            });
        };

        this.action = function(group, action, name, value) {
            if (!config.started) return;
            send(function(data) {
                angular.merge(data, {
                    type: 'a',
                    g: group,
                    a: action,
                    n: name,
                    v: value,
                    u: location.absUrl(),
                    ut: getCurrentTitle(location.absUrl())
                });
            }, null, 'action');
        };

        this.error = function(code, message) {
            if (!config.started) return;
            send(function(data) {
                angular.merge(data, {
                    type: 'e',
                    e: code,
                    m: message
                });
            }, null, 'error');
        };

        function transition(from, to) {
            if (!config.started) return;
            send(function(data) {
                angular.merge(data, {
                    type: 't',
                    f: from,
                    t: to,
                    ft: getTitleByUrl(from)
                });
            }, function(data) {
                data.tt = getCurrentTitle(to);
                if (!data.ft) {
                    data.ft = getTitleByUrl(from);
                }
            }, 'transition');
        }

        function send(before, after, provider) {
            if (!config.started) return;
            var data = getCommonData();
            if (typeof before === 'function') {
                before(data);
            }
            timeout(function() {
                if (typeof after === 'function') {
                    after(data);
                }
                if (typeof config.beforeSendProvider === 'function') {
                    var res = config.beforeSendProvider(provider, data);
                    if (res === false) {
                        return;
                    }
                }
                http({
                    method:'GET',
                    url: config.trackingUrl,
                    ignoreLoadingBar: true,
                    params: data
                });
            }, config.delay);
        }

        function generateClientCode() {
            var result = '';
            var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            for (var i = 8; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
            return result;
        }

        function getCommonData() {
            return {
                aid: config.aid,
                cid: config.clientCode,
                sid: config.sessionCode,
                tm: (new Date()).getTime(),
                ref: config.referrer,
                sr: config.res
            };
        }

        function calculateScreenResolution() {
            if (window.screen) {
                return '' + window.screen.availWidth + 'x' + window.screen.availHeight;
            }
            return null;
        }

        function lsGet(key) {
            if (window.localStorage && typeof window.localStorage.getItem === 'function') {
                return window.localStorage.getItem(config.localStoragePrefix + key);
            }
            return undefined;
        }

        function lsSet(key, value) {
            if (window.localStorage && typeof window.localStorage.setItem === 'function') {
                return window.localStorage.setItem(config.localStoragePrefix + key, value);
            }
        }
    }

    function StatisticDirective(statistic) {
        return {
            restrict: 'A',
            link: function(scope, element) {

                function v(code) {
                    return element.attr("ps-" + code);
                }

                function getFunc() {
                    return function process() {
                        statistic.action(v('category'), v('action'), v('name'), v('value'));
                    };
                }

                if (element.attr('ng-submit')) {
                    element.bind('submit', getFunc());
                } else {
                    element.bind('click', getFunc());
                }

            }
        }
    }

    app.service('$statistic', ['$rootScope', '$http', '$window', '$location', '$timeout', '$document', StatisticService]);
    app.directive('psCategory', ['$statistic', StatisticDirective]);

})(window.angular.module('platform.statistic', []), window.angular);
