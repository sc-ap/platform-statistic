var gulp = require('gulp');
var connect = require('gulp-connect');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');

gulp.task('serve', function() {
    return connect.server({
        port: 4000,
        livereload: false,
        root: '.'
    });
});


gulp.task('build', function() {
    gulp.src('src/platform-statistic.js')
        .pipe(gulp.dest('build'));

    gulp.src('src/platform-statistic.js')
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('build'));
});

gulp.task('watch', function() {
    gulp.watch('src/platform-statistic.js', ['build']);
});

gulp.task('default', ['build', 'serve', 'watch']);